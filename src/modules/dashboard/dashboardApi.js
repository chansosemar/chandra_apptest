import axios from 'axios';

const getContactApi = headers => {
  return axios({
    method: 'GET',
    url: 'https://simple-contact-crud.herokuapp.com/contact',
    headers,
  });
};

const getContactByIdApi = (id, headers) => {
  return axios({
    method: 'GET',
    url: `https://simple-contact-crud.herokuapp.com/contact/${id}`,
    headers,
  });
};

const addContactApi = (data, headers) => {
  return axios({
    method: 'POST',
    url: 'https://simple-contact-crud.herokuapp.com/contact',
    headers: {
      ...headers,
      'Content-Type': 'application/json',
    },
    data,
  });
};
const deleteContactApi = headers => {
  return axios({
    method: 'DELETE',
    url: `https://simple-contact-crud.herokuapp.com/contact/${id}`,
    headers,
  });
};

const editContactByIdApi = (data, headers) => {
  return axios({
    method: 'PUT',
    url: `https://simple-contact-crud.herokuapp.com/contact/${data.id}`,
    data: data.formData,
    headers,
  });
};

export {
  getContactApi,
  getContactByIdApi,
  addContactApi,
  deleteContactApi,
  editContactByIdApi,
};
