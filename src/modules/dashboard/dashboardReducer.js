const initialState = {
  data: [],
  isLoading: false,
  isLoadingModal: false,
  success: '',
  dataById: [],
};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'getContact': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'getContactSuccess': {
      return {
        ...state,
        data: action.payload,
        isLoading: false,
      };
    }
    case 'getContactFailed': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'addContact': {
      return {
        ...state,
        isLoadingModal: true,
      };
    }
    case 'addContactSuccess': {
      return {
        ...state,
        isLoadingModal: false,
        success: action.payload.message,
      };
    }
    case 'addContactFailed': {
      return {
        ...state,
        isLoadingModal: false,
      };
    }
    case 'deleteContact': {
      return {
        ...state,
      };
    }
    case 'deleteContactSuccess': {
      return {
        ...state,
        isLoadingModal: false,
      };
    }
    case 'deleteContactFailed': {
      return {
        ...state,
        isLoadingModal: false,
      };
    }
    case 'getContactById': {
      return {
        ...state,
      };
    }
    case 'getContactByIdSuccess': {
      return {
        ...state,
        dataById: action.payload,
      };
    }
    case 'getContactByIdFailed': {
      return {
        ...state,
      };
    }
    case 'editContactById': {
      return {
        ...state,
        isLoadingModal: true,
      };
    }
    case 'editContactByIdSuccess': {
      return {
        ...state,
        isLoadingModal: false,
      };
    }
    case 'editContactByIdFalse': {
      return {
        ...state,
        isLoadingModal: false,
      };
    }
    default:
      return state;
  }
};

export default dashboardReducer;
