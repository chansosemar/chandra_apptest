import {takeLatest, put} from 'redux-saga/effects';
import {
  getContactApi,
  getContactByIdApi,
  addContactApi,
  deleteContactApi,
  editContactByIdApi,
} from './dashboardApi';
import {getHeaders} from '@/utils/headers';

function* getContact() {
  try {
    const headers = yield getHeaders();
    const res = yield getContactApi(headers);
    yield put({
      type: 'getContactSuccess',
      payload: res.data.data,
    });
  } catch (e) {
    yield put({
      type: 'getContactFailed',
    });
  }
}

function* getContactById(action) {
  try {
    const headers = yield getHeaders();
    const res = yield getContactByIdApi(action.payload, headers);
    yield put({
      type: 'getContactByIdSuccess',
      payload: res.data.data,
    });
  } catch (e) {
    yield put({
      type: 'getContactByIdFailed',
    });
  }
}
function* addContact(action) {
  try {
    const headers = yield getHeaders();
    const res = yield addContactApi(action.payload, headers);
    yield put({
      type: 'addContactSuccess',
      payload: res.data,
    });
  } catch (e) {
    yield put({
      type: 'addContactFailed',
    });
  }
}
function* deleteContact(action) {
  try {
    const headers = yield getHeaders();
    const res = yield deleteContactApi(action.payload, headers);
    yield put({
      type: 'deleteContactSuccess',
      payload: res.data,
    });
  } catch (e) {
    yield put({
      type: 'deleteContactFailed',
    });
  }
}

function* editContactById(action) {
  try {
    const headers = yield getHeaders();
    const res = yield editContactByIdApi(action.payload, headers);
    yield put({
      type: 'editContactByIdSuccess',
    });
  } catch (e) {
    yield put({
      type: 'editContactByIdFailed',
    });
  }
}

function* dashboardSaga() {
  yield takeLatest('getContact', getContact);
  yield takeLatest('getContactById', getContactById);
  yield takeLatest('addContact', addContact);
  yield takeLatest('deleteContact', deleteContact);
  yield takeLatest('editContactById', editContactById);
}

export default dashboardSaga;
