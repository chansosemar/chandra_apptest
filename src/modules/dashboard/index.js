import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  Modal,
  Pressable,
} from 'react-native';
import {hp, wp, color} from 'responsive';
import {useSelector, useDispatch} from 'react-redux';
import {Swipeable} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {UserOne, UserTwo} from 'assets';

const Dashboard = () => {
  const dispatch = useDispatch();
  const [resultData, setResultData] = useState();
  const [searchData, setSearchData] = useState();
  const [modalType, setModalType] = useState();
  const [isModalAddContactOpen, setModalAddContactOpen] = useState(false);
  const [isModalContactByIdOpen, setModalContactByIdOpen] = useState(false);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    age: '',
    photo: '',
  });
  const {isLoading, data, success, isLoadingModal, dataById} = useSelector(
    state => state.dashboardReducer,
  );

  const fetchContact = () => {
    dispatch({type: 'getContact'});
  };
  const findId = () => {
    if (searchData) {
      const res = data.filter(
        e =>
          e.firstName.includes(searchData) || e.lastName.includes(searchData),
      );
      setResultData(res);
    }
  };

  useEffect(() => {
    fetchContact();
  }, []);

  useEffect(() => {
    fetchContact();
    setModalAddContactOpen(false);
    setFormData({
      firstName: '',
      lastName: '',
      age: '',
      photo: '',
    });
  }, [success, isLoadingModal, isModalContactByIdOpen]);

  useEffect(() => {
    findId();
  }, [searchData]);

  useEffect(() => {
    if (!isModalContactByIdOpen && modalType === 'edit') {
      setModalAddContactOpen(true);
    }
  }, [modalType, isModalContactByIdOpen]);

  const RightAction = () => {
    return (
      <View style={styles.swipeContainer('right')}>
        <Text style={styles.swipeText}>Delete</Text>
      </View>
    );
  };

  const RenderItem = e => {
    return (
      <Swipeable
        renderRightActions={RightAction}
        onSwipeableRightOpen={() => {
          dispatch({type: 'deleteContact', payload: e.id});
        }}>
        <TouchableOpacity
          onPress={() => {
            setModalContactByIdOpen(!isModalContactByIdOpen);
            dispatch({type: 'getContactById', payload: e.id});
          }}
          style={styles.contentContainer}>
          <Image
            style={styles.profilePicture}
            source={e.photo === 'N/A' ? UserOne : {uri: e.photo}}
          />
          <View style={styles.subContent}>
            <Text style={styles.title}>
              {e.firstName} {e.lastName}
            </Text>
            <Text style={styles.subTitle}>
              Email :{' '}
              {`${e.firstName.toLowerCase()}.${e.lastName.toLowerCase()}@email.com`}
            </Text>
          </View>
        </TouchableOpacity>
      </Swipeable>
    );
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Search for a person .."
        placeholderTextColor="white"
        style={styles.searchBar}
        value={searchData}
        onChangeText={text => setSearchData(text)}
      />
      <FlatList
        data={searchData?.length > 0 && resultData ? resultData : data}
        keyExtractor={item => item.id}
        renderItem={({item}) => <RenderItem {...item} />}
      />
      {isLoading && (
        <View style={styles.loading}>
          <Text style={{color: color.light}}>Please Wait</Text>
        </View>
      )}
      <TouchableOpacity
        style={styles.floatingButton}
        onPress={() => {
          setModalType('add');
          setModalAddContactOpen(true);
        }}>
        <Icon name="plus" size={30} color={color.submit} />
      </TouchableOpacity>
      <ModalAddContact
        handler={setModalAddContactOpen}
        value={isModalAddContactOpen}
        formData={formData}
        action={setFormData}
        type={modalType}
        data={dataById}
      />
      <ModalContactById
        handler={setModalContactByIdOpen}
        value={isModalContactByIdOpen}
        data={dataById}
        action={setModalType}
        nextAction={setModalAddContactOpen}
      />
    </View>
  );
};

const ModalAddContact = ({handler, value, formData, action, type, data}) => {
  const dispatch = useDispatch();
  const [selected, setSelected] = useState(0);
  const {isLoadingModal} = useSelector(state => state.dashboardReducer);

  const validation = () => {
    if (
      formData.firstName.length > 0 &&
      formData.lastName.length > 0 &&
      formData.age.length > 0 &&
      formData.photo.length > 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  const submitHandler = () => {
    dispatch({type: 'addContact', payload: formData});
  };
  const submitEditHandler = () => {
    dispatch({
      type: 'editContactById',
      payload: {id: data?.id, formData: formData},
    });
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={value}
      onRequestClose={() => {
        handler(!value);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text
            style={{
              color: color.Primary,
              fontWeight: 'bold',
              fontSize: 20,
              alignSelf: 'center',
            }}>
            {type === 'add' ? 'Add Contact' : 'Edit Contact'}
          </Text>
          <InputField
            label="Firstname"
            value={formData.firstName}
            action={text =>
              action({
                ...formData,
                firstName: text,
              })
            }
          />
          <InputField
            label="Lastname"
            value={formData.lastName}
            action={text =>
              action({
                ...formData,
                lastName: text,
              })
            }
          />
          <InputField
            label="Age"
            value={formData.age}
            action={text =>
              action({
                ...formData,
                age: text,
              })
            }
            number
          />
          <View
            style={{
              marginVertical: hp(1),
            }}>
            <Text style={{fontWeight: 'bold'}}>Photo *</Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              {type === 'edit' && (
                <TouchableOpacity
                  onPress={() => {
                    setSelected(0);
                    action({
                      ...formData,
                      photo: data?.photo,
                    });
                  }}>
                  <Image
                    style={
                      selected === 0
                        ? styles.profilePictureActive
                        : styles.profilePicture
                    }
                    source={{
                      uri: data?.photo,
                    }}
                  />
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() => {
                  setSelected(1);
                  action({
                    ...formData,
                    photo: `https://i.pravatar.cc/300?u=a045481f4e29026704d`,
                  });
                }}>
                <Image
                  style={
                    selected === 1
                      ? styles.profilePictureActive
                      : styles.profilePicture
                  }
                  source={{
                    uri: `https://i.pravatar.cc/300?u=a045481f4e29026704d`,
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setSelected(2);
                  action({
                    ...formData,
                    photo: `https://i.pravatar.cc/300?u=a042581f4e29236704d`,
                  });
                }}>
                <Image
                  style={
                    selected === 2
                      ? styles.profilePictureActive
                      : styles.profilePicture
                  }
                  source={{
                    uri: `https://i.pravatar.cc/300?u=a042581f4e29236704d`,
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: hp(2),
            }}>
            <TouchableOpacity
              style={{paddingVertical: hp(2), paddingHorizontal: wp(10)}}
              onPress={() => {
                setSelected(0);
                action({
                  firstName: '',
                  lastName: '',
                  age: '',
                  photo: '',
                });
                handler(!value);
              }}>
              <Text>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                paddingVertical: hp(2),
                paddingHorizontal: wp(10),
                backgroundColor:
                  validation() ? color.muted : color.submit,
                borderRadius: 10,
              }}
              disabled={validation()}
              onPress={type === 'edit' ? submitEditHandler : submitHandler}>
              {isLoadingModal ? (
                <ActivityIndicator size="small" color={color.light} />
              ) : (
                <Text style={{color: color.light}}>Submit</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const ModalContactById = ({handler, value, data, action, nextAction}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={value}
      onRequestClose={() => {
        handler(!value);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <View style={{alignSelf: 'center'}}>
            <View style={{padding: wp(2)}}>
              <Image
                style={styles.profilePicture}
                source={{
                  uri: data?.photo,
                }}
              />
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: wp(0.5),
                  right: wp(0.5),
                }}
                onPress={() => {
                  handler(!value);
                  action('edit');
                }}>
                <Icon name="pencil" size={20} color={color.submit} />
              </TouchableOpacity>
            </View>
          </View>
          <LabelField label="Firstname" data={data?.firstName} />
          <LabelField label="Lastname" data={data?.lastName} />
          <LabelField
            label="Email"
            data={`${data?.firstName?.toLowerCase()}.${data?.lastName?.toLowerCase()}@email.com`}
          />
          <LabelField label="Age" data={data?.age} />
          <TouchableOpacity
            style={{
              paddingVertical: hp(2),
              paddingHorizontal: wp(10),
              alignSelf: 'center',
            }}
            onPress={() => {
              action('');
              handler(!value);
            }}>
            <Text>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const InputField = ({label, action, value, number}) => {
  return (
    <>
      <Text style={{fontWeight: 'bold'}}>{label} *</Text>
      <TextInput
        value={value}
        placeholder={`Type your ${label}`}
        style={styles.textInputModal}
        onChangeText={action}
        keyboardType={number && 'numeric'}
      />
    </>
  );
};
const LabelField = ({label, data}) => {
  return (
    <View style={{marginVertical: hp(1)}}>
      <Text style={{fontWeight: 'bold'}}>{label} *</Text>
      <Text>{data}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.primary,
    height: hp(100),
  },
  searchBar: {
    backgroundColor: color.secondary,
    padding: hp(1),
    borderRadius: hp(1),
    color: color.light,
  },
  profilePicture: {
    width: wp(15),
    height: wp(15),
    resizeMode: 'cover',
    borderRadius: hp(10),
  },
  profilePictureActive: {
    width: wp(15),
    height: wp(15),
    resizeMode: 'cover',
    borderRadius: hp(10),
    borderWidth: 3,
    borderColor: color.warning,
  },
  title: {
    color: color.light,
    fontSize: hp(2.5),
  },
  subTitle: {
    color: color.grey,
    fontSize: hp(1.5),
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: hp(2),
    borderBottomWidth: 2,
    borderBottomColor: color.secondary,
  },
  subContent: {
    marginLeft: wp(5),
  },
  loading: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: hp(50),
    right: wp(40),
  },
  swipeContainer: e => ({
    backgroundColor: e === 'right' ? color.danger : color.success,
    paddingHorizontal: wp(10),
    alignItems: 'center',
    justifyContent: 'center',
  }),
  swipeText: {
    color: color.light,
    fontWeight: 'bold',
  },
  floatingButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(15),
    position: 'absolute',
    bottom: wp(7.5),
    right: wp(2.5),
    height: wp(15),
    backgroundColor: color.light,
    borderRadius: 100,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 10,
    padding: wp(5),
    width: wp(70),
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  textInputModal: {
    backgroundColor: color.light,
    padding: hp(1),
    marginVertical: hp(1),
    borderRadius: hp(1),
    color: color.primary,
  },
});

export default Dashboard;
