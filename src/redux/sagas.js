import {all} from 'redux-saga/effects';
import dashboardSaga from 'modules/dashboard/dashboardSaga';

export default function* rootSaga() {
  yield all([dashboardSaga()]);
}
