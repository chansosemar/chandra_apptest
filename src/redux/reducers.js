import {combineReducers} from 'redux';
import dashboardReducer from 'modules/dashboard/dashboardReducer';

export default combineReducers({
  dashboardReducer,
});
